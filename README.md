# lynarstudiosweb-express

##### install dependencies:
$ npm install

##### run the app:
$ DEBUG=lynarstudioweb-express:* npm start

##### open in the browser:
http://localhost:3000
