const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const gamesRouter = require('./routes/games');
const legalnoticeRouter = require('./routes/legal_notice');
const privacyRouter = require('./routes/privacy');
const recruitmentRouter = require('./routes/recruitment');
const toolsRouter = require('./routes/tools');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/games', gamesRouter);
app.use('/legalnotice', legalnoticeRouter);
app.use('/privacy', privacyRouter);
app.use('/recruitment', recruitmentRouter);
app.use('/tools', toolsRouter);

// catch 404
app.use(function(req, res) {
  res.status(404);
  res.render('error');
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
