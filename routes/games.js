const express = require('express');
const router = express.Router();

/* GET solar light page. */
router.get('/solarlight', function(req, res) {
  res.render('games/solar_light', { title: 'Solar Light' });
});

/* GET rack wheel city page. */
router.get('/rackwheelcity', function(req, res) {
  res.render('games/rack_wheel_city', { title: 'Rack Wheel City' });
});

/* GET games root page. */
router.get('/', function(req, res) {
  res.render('games/index', { title: 'Games' });
});

module.exports = router;
