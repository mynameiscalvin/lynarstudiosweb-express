const express = require('express');
const router = express.Router();

/* GET legal notice page. */
router.get('/', function(req, res) {
  res.render('legal_notice', { title: 'Legal Notice' });
});

module.exports = router;
