const express = require('express');
const router = express.Router();

/* GET privacy page. */
router.get('/', function(req, res) {
  res.render('privacy', { title: 'Privacy' });
});

module.exports = router;
