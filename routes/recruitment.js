const express = require('express');
const router = express.Router();

/* GET graphics designer page. */
router.get('/graphicsdesigner', function(req, res) {
  res.render('recruitment/graphics_designer', { title: 'Graphics Designer' });
});

/* GET software engineer page. */
router.get('/softwareengineer', function(req, res) {
  res.render('recruitment/software_engineer', { title: 'Software Engineer' });
});

/* GET story teller page. */
router.get('/storyteller', function(req, res) {
  res.render('recruitment/story_teller', { title: 'Story Teller' });
});

/* GET recruitment root page. */
router.get('/', function(req, res) {
  res.render('recruitment/index', { title: 'Recruitment' });
});

module.exports = router;
