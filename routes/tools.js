const express = require('express');
const router = express.Router();

/* GET lynar engine moon page. */
router.get('/lynarenginemoon', function(req, res) {
  res.render('tools/lynar_engine_moon', { title: 'Lynar Engine Moon' });
});

/* GET lynar engine neptune page. */
router.get('/lynarengineneptune', function(req, res) {
  res.render('tools/lynar_engine_neptune', { title: 'Lynar Engine Neptune' });
});

/* GET ls standard library page. */
router.get('/lsstandardlibrary', function(req, res) {
  res.render('tools/ls_standard_library', { title: 'LS Standard Library' });
});

/* GET tools root page. */
router.get('/', function(req, res) {
  res.render('tools/index', { title: 'Tools' });
});

module.exports = router;
